package business_layer.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.buf.UDecoder;
import org.hibernate.cfg.Configuration;

import data_access_layer.daos.FlightDAO;
import data_access_layer.models.Flight;

/**
 * Servlet implementation class FlightServlet
 */
@WebServlet("/FlightServlet")
public class FlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FlightServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
	
		if (request.getParameter("delete_f")!=null){
			String idToDelete =request.getParameter("id");
			@SuppressWarnings("unused")
			int test = Integer.parseInt(idToDelete);
			FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
			fDao.removeFlight(Integer.parseInt(idToDelete));
			PrintWriter out = response.getWriter();
			out.println("Deleted!");
		}else if (request.getParameter("update_f")!=null){
		String idToUpdate = request.getParameter("id");
		PrintWriter out = response.getWriter();
		String flightNb=request.getParameter("flight_nb");
		String airplaneType=request.getParameter("airplane_type");
		String departureTime=request.getParameter("departure_time");
		String arrivalTime=request.getParameter("arrival_time");
		String departureCityId=request.getParameter("departure_city_id");
		String arrival_city_id=request.getParameter("arrival_city_id");
		out.println("Added!"+flightNb+", "+airplaneType+", "+departureTime
				+", "+arrivalTime+", "+departureCityId+", "+
				arrival_city_id);
		Date dArr = new Date();
		Date dDep = new Date();
		

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			dArr = sdf.parse(arrivalTime);
			dDep = sdf.parse(departureTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Flight newFlight = new Flight(1, flightNb, airplaneType, 1, 2, dDep, dArr);
		FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
		fDao.updateFlight(fDao.findFlight(Integer.parseInt(idToUpdate)), newFlight);
		
		out.println("Updated!");
		}else if (request.getParameter("add_f")!=null){
		PrintWriter out = response.getWriter();
		
		String flightNb=request.getParameter("flight_nb");
		String airplaneType=request.getParameter("airplane_type");
		String departureTime=request.getParameter("departure_time");
		String arrivalTime=request.getParameter("arrival_time");
		String departureCityId=request.getParameter("departure_city_id");
		String arrival_city_id=request.getParameter("arrival_city_id");
		out.println("Added!"+flightNb+", "+airplaneType+", "+departureTime
				+", "+arrivalTime+", "+departureCityId+", "+
				arrival_city_id);
		
		//2015-01-10t13:0/
		

		Date dArr = new Date();
		Date dDep = new Date();
		

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		try {
			dArr = sdf.parse(arrivalTime);
			dDep = sdf.parse(departureTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Flight newFlight = new Flight(1, flightNb, airplaneType, 1, 2, dDep, dArr);
		FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
		fDao.addFlight(newFlight);
		}
	}

}
