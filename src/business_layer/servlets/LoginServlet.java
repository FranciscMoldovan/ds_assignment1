package business_layer.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import data_access_layer.daos.UserDAO;
import data_access_layer.models.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse
			response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			String user =request.getParameter("user");
			String pass =request.getParameter("pass");
			//insert student into database
			//insert(firstName,lastName,cnp);
			//create response - HTML page
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out = response.getWriter();
			try {
				 UserDAO uDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
				 User loginUser = new User();
				 loginUser=uDAO.findUser(user, pass);
			if (loginUser!=null){
				// LOGINT OKAY
				//out.println("<h2> Welcome "+loginUser.getFirstName()+" "+loginUser.getLastName()+"</h2>");
				HttpSession session = request.getSession();
				session.setAttribute("user", user);
				session.setAttribute("role", loginUser.getRole());
				// setting session to expiry in 30 mins
				session.setMaxInactiveInterval(30*60);
				Cookie userName = new Cookie("user", user);
				Cookie role = new Cookie("role", loginUser.getRole());
				userName.setMaxAge(30*60);
				role.setMaxAge(30*60);
				response.addCookie(userName);
				response.addCookie(role);
				if (loginUser.getRole().equals("admin")){
					response.sendRedirect("LoginSuccessAdmin.jsp");
				}
				else if (loginUser.getRole().equals("user"))
				{
					response.sendRedirect("LoginSuccessUser.jsp");
				}
				
			}
			
			else{
			//servlet code
			//PrintWriter out = response.getWriter();  
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.html");
			rd.include(request, response);
			response.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('WRONG CREDENTIALS!!!');");  
			out.println("</script>");
			}
			} finally {
			out.close();
			}
			}

}
