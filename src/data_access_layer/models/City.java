package data_access_layer.models;

import java.util.Date;

public class City {
	private int id;
	private String name;
	private double latitude;
	private double longitude;
	private Date localTime;
	public City(){}
	public City(int id,String name, double d, double e, Date lT){
		this.id=id;
		this.name=name;
		this.latitude=d;
		this.longitude=e;
		this.localTime=lT;
	}
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	
	public Date getLocalTime() {
		return localTime;
	}
	public void setLocalTime(Date localTime) {
		this.localTime = localTime;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	@Override
	public String toString() {
		return "City [id=" + id + ", latitude=" + latitude + ", longitude=" + longitude + ", localTime=" + localTime
				+ "]";
	}

	
}

