package data_access_layer.models;

import java.util.Date;

public class Flight {
	private int id;
	private String flighNb; 
	private String airplaneType;
	private int departureCityID; 
	private int arrivalCityID;
	private Date departureTime;
	private Date arrivalTime;
	
	public Flight(){}
	public Flight(int id, String fN, String arT, int dC, int aC, Date dT, Date arrT){
		this.id=id;
		this.flighNb=fN; 
		this.airplaneType=arT; 
		this.departureCityID=dC;
		this.arrivalCityID=aC;
		this.departureTime=dT;
		this.arrivalTime=arrT;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFlighNb() {
		return flighNb;
	}
	public void setFlighNb(String flighNb) {
		this.flighNb = flighNb;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getDepartureCityID() {
		return departureCityID;
	}
	public void setDepartureCityID(int departureCityID) {
		this.departureCityID = departureCityID;
	}
	public int getArrivalCityID() {
		return arrivalCityID;
	}
	public void setArrivalCityID(int arrivalCityID) {
		this.arrivalCityID = arrivalCityID;
	}
	@Override
	public String toString() {
		return "Flight [id=" + id + ", flighNb=" + flighNb + ", airplaneType=" + airplaneType + ", departureCityID="
				+ departureCityID + ", arrivalCityID=" + arrivalCityID + ", departureTime=" + departureTime
				+ ", arrivalTime=" + arrivalTime + "]";
	}
	
	

}

