package data_access_layer.models;

public class User {
	private int id; 
	private String firstName; 
	private String lastName; 
	private String userName; 
	private String password; 
	private String role; 
	
	public User(){}
	public User(int id, String fN, String lN, String uN, String pW, String rL )
	{
		this.id=id; 
		this.firstName=fN; 
		this.lastName=lN;
		this.userName=uN;
		this.password=pW;
		this.role=rL;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName
				+ ", password=" + password + ", role=" + role + "]";
	}
	
	
	
}

