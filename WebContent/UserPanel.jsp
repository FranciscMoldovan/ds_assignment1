<%@page import="data_access_layer.daos.CityDAO"%>
<%@page import="data_access_layer.models.Flight"%>
<%@page import="data_access_layer.models.City"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>
<%@page import="data_access_layer.daos.FlightDAO"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Login Success Page</title>
</head>
<body>
<%
//allow access only if session exists
if(session.getAttribute("user") == null){
	response.sendRedirect("index.html");
}
String userName = null;
String role = null;
String sessionID = null;
Cookie[] cookies = request.getCookies();
if(cookies !=null){
for(Cookie cookie : cookies){
	if(cookie.getName().equals("user")) userName = cookie.getValue();
	if(cookie.getName().equals("role")) role = cookie.getValue();
}
}
%>
<h3>Hi <%=userName %>, ROLE: <%= role %>, do the checkout.</h3>
<h3>you can perform Flight CRUD operations.</h3>
<form action="LogoutServlet" method="post">
<input type="submit" value="Logout" >
</form>
<br>
<%
	FlightDAO fDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
	List<Flight> myList = null;
	myList = fDao.findFlights();
	Flight a = myList.get(0);
	
	 CityDAO cDao = new CityDAO(new Configuration().configure().buildSessionFactory());
%>

<table border="1">
            <thead>
                <tr>
                	<th>Flight ID</th>
                    <th>Flight Number</th>
                    <th>Airplane type </th>
                    <th>departure Time</th>
                    <th>Arrival Time </th>
                    <th>Departure city id </th>
                    <th>Departure city Name </th>
                    <th>Arrival city id </th>
                    <th>Arrival city name</th>
                </tr>
            </thead>
            <tbody>
                <%
                    for (Flight flight : myList) {
                %>
                <TR>
                    <TD> <%= flight.getId() %> </TD>
                    <TD> <%= flight.getFlighNb()%> </TD>
                    <TD> <%= flight.getAirplaneType()%> </TD>
                    <TD> <%= flight.getDepartureTime()%> </TD>
                    <TD> <%= flight.getArrivalTime()%> </TD>
                    <TD> <%= flight.getDepartureCityID()%> </TD>
                    <TD> <%= cDao.findCity(flight.getDepartureCityID()).getName()  %>  </TD>
                    <TD> <%= flight.getArrivalCityID()%> </TD>
                   <TD> <%= cDao.findCity(flight.getArrivalCityID()).getName()  %>  </TD>
                </TR>
                <%
                    }
                %>
            </tbody>
        </table>
</body>
</html>










